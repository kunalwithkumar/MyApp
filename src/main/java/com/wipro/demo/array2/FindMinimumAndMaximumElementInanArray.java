package com.wipro.demo.array2;

import java.util.Arrays;

public class FindMinimumAndMaximumElementInanArray {

//	Find minimum and maximum element in an array
//	BasicAccuracy: 68.55%Submissions: 100k+Points: 1
//	Given an array A of size N of integers. Your task is to find the minimum and maximum elements in the array.
//
//	 
//
//	Example 1:
//
//	Input:
//	N = 6
//	A[] = {3, 2, 1, 56, 10000, 167}
//	Output:
//	min = 1, max =  10000
//	 
//
//	Example 2:
//
//	Input:
//	N = 5
//	A[]  = {1, 345, 234, 21, 56789}
//	Output:
//	min = 1, max = 56789
//	 
//
//	Your Task:  
//	You don't need to read input or print anything. Your task is to complete the function getMinMax() which takes the array A[] and its size N as inputs and returns the minimum and maximum element of the array.
//
//	 
//
//	Expected Time Complexity: O(N)
//	Expected Auxiliary Space: O(1)

	public static void main(final String[] args) {

		final long a[] = { 1, 345, 234, 21, 56789 };
		System.out.println(getMinMax(a, a.length));

	}

	static pair getMinMax(final long a[], final long n) {
		Arrays.sort(a);
		return new pair(a[0], a[a.length - 1]);
	}

}

class pair {
	long first, second;

	public pair(final long first, final long second) {
		this.first = first;
		this.second = second;
	}
}
