package com.wipro.demo.array2;

import java.util.Arrays;

public class CheckIfTwoArraysAreEqualOrNot {

//	Check if two arrays are equal or not
//	BasicAccuracy: 42.18%Submissions: 100k+Points: 1
//	Lamp
//	This problem is part of GFG SDE Sheet. Click here to view more.  
//
//	Given two arrays A and B of equal size N, the task is to find if given arrays are equal or not. Two arrays are said to be equal if both of them contain same set of elements, arrangements (or permutation) of elements may be different though.
//	Note : If there are repetitions, then counts of repeated elements must also be same for two array to be equal.
//
//	Example 1:
//
//	Input:
//	N = 5
//	A[] = {1,2,5,4,0}
//	B[] = {2,4,5,0,1}
//	Output: 1
//	Explanation: Both the array can be 
//	rearranged to {0,1,2,4,5}
//	Example 2:
//
//	Input:
//	N = 3
//	A[] = {1,2,5}
//	B[] = {2,4,15}
//	Output: 0
//	Explanation: A[] and B[] have only 
//	one common value.

	public static void main(final String[] args) {

		final int A[] = { 1, 2, 5, 4, 0 };
		final int B[] = { 2, 4, 5, 77, 1 };

		Arrays.sort(A);
		Arrays.sort(B);
		for (int i = 0; i < A.length; i++) {
			if (A[i] != B[i]) {
				System.out.println("no");

			} else {
				System.out.println("yes");
			}
		}
	}

	public static boolean check(final long A[], final long B[], final int N) {
		// Your code here

		Arrays.sort(A);
		Arrays.sort(B);
		for (int i = 0; i < A.length; i++) {
			if (A[i] != B[i])
				return false;
		}
		return true;
	}
}
