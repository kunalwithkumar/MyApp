package com.wipro.demo.array2;

import java.util.Arrays;

public class BinarySearch {
//	
//	Binary Search
//	BasicAccuracy: 44.32%Submissions: 100k+Points: 1
//	Lamp
//	This problem is part of GFG SDE Sheet. Click here to view more.  
//
//	Given a sorted array of size N and an integer K, find the position at which K is present in the array using binary search.
//
//	Example 1:
//
//	Input:
//	N = 5
//	arr[] = {1 2 3 4 5} 
//	K = 4
//	Output: 3
//	Explanation: 4 appears at index 3.
//
//	Example 2:
//
//	Input:
//	N = 5
//	arr[] = {11 22 33 44 55} 
//	K = 445
//	Output: -1
//	Explanation: 445 is not present.

	public static void main(final String[] args) {
		// TODO Auto-generated method stub

		final int arr[] = { 1, 2, 3, 4, 5 };
		final int val = Arrays.binarySearch(arr, 66);
		System.out.println(Arrays.binarySearch(arr, 66));
		if (val < 0) {
			System.out.println("no");
		} else {
			System.out.println("yews");
		}
	}

	int binarysearch(final int arr[], final int n, final int k) {
		// code here

		final int val = Arrays.binarySearch(arr, k);
		if (val < 0)
			return -1;

		return val;
	}

}
