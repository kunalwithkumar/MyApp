package com.wipro.demo.array2;

import java.util.Arrays;

public class LargestElementInArray {

//	Largest Element in Array
//	BasicAccuracy: 67.48%Submissions: 100k+Points: 1
//	Given an array A[] of size n. The task is to find the largest element in it.
//	 
//
//	Example 1:
//
//	Input:
//	n = 5
//	A[] = {1, 8, 7, 56, 90}
//	Output:
//	90
//	Explanation:
//	The largest element of given array is 90.
//	 
//
//	Example 2:
//
//	Input:
//	n = 7
//	A[] = {1, 2, 0, 3, 2, 4, 5}
//	Output:
//	5
//	Explanation:
//	The largest element of given array is 5.
//	 
//
//	Your Task:  
//	You don't need to read input or print anything. Your task is to complete the function largest() which takes the array A[] and its size n as inputs and returns the maximum element in the array.

	public static void main(final String[] args) {
		final int arr[] = { 1, 8, 7, 56, 90 };
		System.out.println(largest(arr, arr.length));

	}

	public static int largest(final int arr[], final int n) {
		Arrays.sort(arr);
		return arr[n - 1];
	}

}
