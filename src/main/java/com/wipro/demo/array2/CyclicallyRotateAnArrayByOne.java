package com.wipro.demo.array2;

public class CyclicallyRotateAnArrayByOne {

//	Cyclically rotate an array by one
//	BasicAccuracy: 69.6%Submissions: 100k+Points: 1
//	Given an array, rotate the array by one position in clock-wise direction.
//	 
//
//	Example 1:
//
//	Input:
//	N = 5
//	A[] = {1, 2, 3, 4, 5}
//	Output:
//	5 1 2 3 4
//	 
//
//	Example 2:
//
//	Input:
//	N = 8
//	A[] = {9, 8, 7, 6, 4, 2, 1, 3}
//	Output:
//	3 9 8 7 6 4 2 1

	public static void main(final String[] args) {
		final int arr[] = { 1, 2, 3, 4, 5 };

		final int x = arr[arr.length - 1];

		for (int i = arr.length - 1; i > 0; i--) {
			arr[i] = arr[i - 1];
		}
		arr[0] = x;

		for (final int element : arr) {
			System.out.println(element);
		}

	}

}
