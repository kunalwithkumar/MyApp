package com.wipro.demo.algorithm;

import java.util.Arrays;

public class PrintWithoutLoop {

	public static void main(final String[] args) {
		// TODO Auto-generated method stub
		// printNos(5);
		// armstrongNumber(111);
		System.out.println(count_divisors(4));

	}

	static public void printNos(final int N) {
		// Your code here
		if (N < 1)
			return;
		printNos(N - 1);

		System.out.print(N + " ");
	}

	public int find_median(final int[] v) {
		// Code here
		Arrays.sort(v);
		final int len = v.length;
		if ((len % 2) == 0)
			return ((v[len / 2] + v[(len - 1) / 2]) / 2);
		return v[len / 2];
	}

	static String armstrongNumber(int n) {
		// code here

		final int num = n;
		int sum = 0, rem;
		while (n > 0) {
			rem = n % 10;

			sum = sum + (rem * rem * rem);
			n = n / 10;

		}
		if (num == sum) {
			System.out.println("yes");
		} else {
			System.out.println("no");
		}

		return "";

	}

	static long count_divisors(final int N) {
		// code here
		long c = 0;
		for (int i = 1; i <= N; i++) {
			if (((N % i) == 0) && ((i % 3) == 0)) {
				c = c + 1L;
			}
			// integers.add(i);
		}

		return c;
	}

	static public long reverse_digit(long n) {

		// Code here

		final long num = n;
		final StringBuilder s = new StringBuilder();
		long rem;
		while (n > 0) {
			rem = n % 10;
			s.append(rem);

			// sum = sum + (rem * rem * rem);
			n = n / 10;

		}
		System.out.println(s.toString());
		return num;
	}

}
