package com.wipro.demo.algorithm;

import java.util.ArrayList;
import java.util.Arrays;

public class Fibonacci {

	public static void main(final String[] args) {
		// TODO Auto-generated method stub
		final long arr[] = { 1, 1, 2, 2, 3, 4, 5 };
//		System.out.println(Maximize(arr, arr.length));
		// System.out.println((999 * 9 * 9) / 100.0);
		// System.out.println(product(arr, 7));
//		int c = 1;
//		for (int i = 0; i < 5; i++) {
//			for (int j = 0; j < i; j++) {
//				// if (j < c) {
//				System.out.print(c);
//				// c++;
//				// }
//				// c++;
//
//			}
//			c++;
//			System.out.println();
//		}
		ArrayList<Long> list = new ArrayList<>();
		list = find(arr, arr.length, 5);
		System.out.println(list.get(0));
		System.out.println(list.get(1));

	}

	static ArrayList<Long> find(final long arr[], final int n, final int x) {
		// code here
		long first = -1L;
		long second = -1L;
		for (int i = 0; i < n; i++) {
			if (((arr[i] == x) && (first == -1))) {
				first = i;
				// second = i;
				if ((arr[i + 1] == x)) {
					second = i + 1;
				}

			}
			if (second == -1) {
				second = first;
			}

		}
		final ArrayList<Long> list = new ArrayList<>();
		list.add(first);
		list.add(second);
		return list;
	}

	public static long[] printFibb(final int n) {
		// Your code here
		final long a[] = new long[n];
		if (n <= 1) {
			a[0] = 1;
		} else if (n == 2) {
			a[0] = a[1] = 1;
		} else {
			a[0] = a[1] = 1;

			for (int i = 2; i < n; i++) {
				a[i] = a[i - 2] + a[i - 1];
			}
		}
		return a;
	}

	static int Maximize(final int arr[], final int n) {
		// Complete the function
		Arrays.sort(arr);
		int sum = 0;
		for (int i = 0; i < n; i++) {
			final int res = i * arr[i];
			// System.out.println(sum);
			sum += res;
		}
		return sum;

	}

	public static int product(final int arr[], final int n) {
		int pro = 1;
		for (int i = 0; i < n; i++) {
			pro *= arr[i];
		}
		return pro;
	}
}
