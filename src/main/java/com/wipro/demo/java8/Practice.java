package com.wipro.demo.java8;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.wipro.demo.java.PrimeCheck;

@FunctionalInterface
interface Test {

	void test();

	default void hello() {
		System.out.println("Hello world from hello");
	}

	static void testMethod() {
		System.out.println("Hello From static Test Method");
	}
}

public class Practice {

	public static void main(final String[] args) {

		final Test test = () -> {
			System.out.println("Hello World");
		};

		Test.testMethod();

		test.test();

		test.hello();

		final List<Integer> list = new ArrayList<>();

		for (int i = 0; i < 10; i++) {
			list.add(i);
		}

		list.stream().map(i -> i * 3)
				// .forEach(System.out::println);
				.forEach(i -> {
					System.out.println(i);
				});

		final List<String> strings = new ArrayList<>();

		strings.add("Hello");
		strings.add("World");
		strings.add("From");
		strings.add("Kunal");

		final List<String> collect = strings.stream().sorted().map(String::toUpperCase).collect(Collectors.toList());
		// .forEach(System.out::println);

		collect.stream().forEach(System.out::println);

//		strings.stream().flatMap(word -> Stream.of(word.concat(" concat").toUpperCase())).forEach(word -> {
//			System.out.println(word);
//		});

		final List<Integer> primeList = new ArrayList<>();
		final List<Integer> evenList = new ArrayList<>();
		final List<Integer> oddList = new ArrayList<>();

		for (int i = 0; i < 50; i++) {
			if (PrimeCheck.isPrime(i)) {
				primeList.add(i);
			}
			if ((i % 2) == 0) {
				evenList.add(i);
			} else {
				oddList.add(i);
			}
		}

		System.out.println("prime------>>>>");
		primeList.stream().forEach(System.out::println);
		System.out.println("even ---->>>");
		evenList.stream().forEach(System.out::println);
		System.out.println("odd ----->>>");
		oddList.stream().forEach(System.out::println);

		System.out.println("list off all is " + (primeList.size() + evenList.size() + oddList.size()));

		final List<List<Integer>> listOfInteger = Arrays.asList(primeList, evenList, oddList);

		final List<Integer> finaList = listOfInteger.stream().flatMap(List::stream).collect(Collectors.toList());

		System.out.println("Before final List");
		System.out.println(finaList.size());

		System.out.println("Elements are----------");
		finaList.stream().forEach(System.out::println);

		final LocalDate date = LocalDate.now();
		System.out.println(date);

		final LocalTime time = LocalTime.now();
		System.out.println(time);

		final LocalDateTime dateTime = LocalDateTime.now();
		System.out.println(dateTime);

	}

}
