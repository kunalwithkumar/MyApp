package com.wipro.demo.array;

public class AlternatePrint {

//	Print alternate elements of an array
//	SchoolAccuracy: 52.74%Submissions: 100k+Points: 0
//	You are given an array A of size N. You need to print elements of A in alternate order (starting from index 0).
//
//	Example 1:
//
//	Input:
//	N = 4
//	A[] = {1, 2, 3, 4}
//	Output:
//	1 3
//	Example 2:
//
//	Input:
//	N = 5
//	A[] = {1, 2, 3, 4, 5}
//	Output:
//	1 3 5
//	Your Task:
//	Since this is a function problem, you just need to complete the provided function void print(int ar[],int n)
//
//	Constraints:
//	1 <= N <= 105
//	1 <= Ai <= 105
//
//	Expected Time Complexity: O(n)
//	Expected Auxiliary Space: O(1)

	public static void print(final int arr[], final int n) {
		// your code here
		for (int i = 0; i < n; i = i + 2) {
			System.out.print(arr[i] + " ");
		}
	}

}
