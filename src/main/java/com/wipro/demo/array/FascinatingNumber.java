package com.wipro.demo.array;

import java.util.HashSet;
import java.util.Set;

public class FascinatingNumber {

//	Fascinating Number
//	SchoolAccuracy: 31.53%Submissions: 19511Points: 0
//	Given a number N. Your task is to check whether it is fascinating or not.
//	Fascinating Number: When a number(should contain 3 digits or more) is multiplied by 2 and 3 ,and when both these products are concatenated with the original number, then it results in all digits from 1 to 9 present exactly once.
//
//	Example 1:
//
//	Input: 
//	N = 192
//	Output: Fascinating
//	Explanation: After multiplication with 2
//	and 3, and concatenating with original
//	number, number will become 192384576 
//	which contains all digits from 1 to 9.
//	Example 2:
//
//	Input: 
//	N = 853
//	Output: Not Fascinating
//	Explanation: It's not a fascinating
//	number.

	public static void main(final String[] args) {
		System.out.println(fascinating(192L));

	}

	static boolean fascinating(final long n) {
		// code here

		final long mul2 = n * 2;
		final long mul3 = n * 3;

		final String str = mul2 + "" + mul3 + n;
		boolean flag = false;

		System.out.println(str);
		if (str.length() == 9) {
			final Set<Character> characters = new HashSet<>();
			for (int i = 0; i < 9; i++) {
				final Character ch = str.toCharArray()[i];
				System.out.println(ch);
				flag = characters.add(ch);
				if (!flag)
					return false;

			}
		} else {
			flag = false;
		}

		return flag;
	}

}
