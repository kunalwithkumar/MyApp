package com.wipro.demo.array;

public class PerfectArray {

//	Perfect Arrays
//	SchoolAccuracy: 43.54%Submissions: 28965Points: 0
//	Given an array of size N and you have to tell whether the array is perfect or not. An array is said to be perfect if it's reverse array matches the original array. If the array is perfect then print "PERFECT" else print "NOT PERFECT".
//
//	Example 1:
//
//	Input : Arr[] = {1, 2, 3, 2, 1}
//	Output : PERFECT
//	Explanation:
//	Here we can see we have [1, 2, 3, 2, 1] 
//	if we reverse it we can find [1, 2, 3, 2, 1]
//	which is the same as before.
//	So, the answer is PERFECT.
//
//	Example 2:
//
//	Input : Arr[] = {1, 2, 3, 4, 5}
//	Output : NOT PERFECT
//
//	User Task:
//	The task is to complete the function IsPerfect(), which takes an array (a), size of the array (n), and return the boolean value true if it Perfect else false. The drivercode automatically adds a new line.
//
//	Expected Time Complexity: O(N).
//	Expected Auxiliary Space: O(1).
//
//	Constraints:
//	1 ≤ N ≤ 105
//	1 ≤ ai ≤ 103

	public static void main(final String[] args) {

		final int arr[] = { 15, 20, 5, 6, 5, 6, 13, 4, 3, 4, 13, 6, 5, 6, 5, 20, 15 };

		final int len = arr.length;
		// for (int i = 0; i < (len / 2); i++) {
//			left[i] = arr[i];
//		}
//
//		for (int j = 0; j < (len / 2); j++) {
//			len--;
//			right[j] = arr[len];
//
//		}
//		for (int i = 0; i < (len / 2); i++) {
//			if (left[i] != right[i]) {
//				System.out.println("not");
//				break;
//			}
//
//		}
		boolean flag = true;
		int j = len - 1, i = 0;
		for (int k = 0; k < (len / 2); k++) {
			if (arr[i] != arr[j]) {
				flag = false;
				break;
			}
			i++;
			j--;
		}
		System.out.println(flag);
	}

}
