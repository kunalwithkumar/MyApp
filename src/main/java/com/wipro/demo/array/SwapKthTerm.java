package com.wipro.demo.array;

public class SwapKthTerm {

//	Swap kth elements
//	SchoolAccuracy: 52.76%Submissions: 45678Points: 0
//	Given an array Arr of size N, swap the Kth element from beginning with Kth element from end.
//
//	Example 1:
//
//	Input:
//	N = 8, K = 3
//	Arr[] = {1, 2, 3, 4, 5, 6, 7, 8}
//	Output: 1 2 6 4 5 3 7 8
//	Explanation: Kth element from beginning is
//	3 and from end is 6.
//	Example 2:
//
//	Input:
//	N = 5, K = 2
//	Arr[] = {5, 3, 6, 1, 2}
//	Output: 5 1 6 3 2
//	Explanation: Kth element from beginning is
//	3 and from end is 1.
//	Your Task:
//	You don't need to read input or print anything. Your task is to complete the function swapKth() which takes the array of integers arr, n and k as parameters and returns void. You have to modify the array itself.
//
//	Expected Time Complexity: O(1)
//	Expected Auxiliary Space: O(1)
//
//	Constraints:
//	1 ≤ K ≤ N ≤ 105
//	1 ≤ Arr[i] ≤ 103

	void swapKth(final int arr[], final int n, final int k) {
		final int first = arr[k - 1];
		final int sec = arr[arr.length - k];

		arr[k - 1] = sec;
		arr[arr.length - k] = first;

	}

	public static void main(final String[] args) {

		final SwapKthTerm kthTerm = new SwapKthTerm();
		final int arr[] = { 5, 3, 6, 1, 2 };

		kthTerm.swapKth(arr, 5, 2);
		for (int i = 0; i < 5; i++) {
			System.out.print(arr[i] + " ");
		}
	}

}
