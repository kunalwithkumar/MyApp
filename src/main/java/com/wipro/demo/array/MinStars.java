package com.wipro.demo.array;

import java.util.Arrays;

public class MinStars {

	public static void main(final String[] args) {

		final int arr[] = { 1, 1, 1, 4, 1 };
		int count = 0, val = 0;
		Arrays.sort(arr);

		for (int j = 1; j < arr.length; j++) {
			// System.out.println(j + "j is");
			if (arr[0] == arr[j]) {
				val++;
				arr[j] = arr[j] + val;
				count += val;
				System.out.println(val + " val");

			}
		}

		System.out.println((count + "count"));
		for (final int element : arr) {
			System.out.println(element);
		}
	}

}
