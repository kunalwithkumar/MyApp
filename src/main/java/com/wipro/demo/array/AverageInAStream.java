package com.wipro.demo.array;

public class AverageInAStream {

//	Average in a stream
//	SchoolAccuracy: 49.93%Submissions: 19393Points: 0
//	Given a stream of incoming numbers, find average or mean of the stream at every point.
//
//	 
//
//	Example 1:
//
//	Input:
//	n = 5
//	arr[] = {10, 20, 30, 40, 50}
//	Output: 10.00 15.00 20.00 25.00 30.00 
//	Explanation: 
//	10 / 1 = 10.00
//	(10 + 20) / 2 = 15.00
//	(10 + 20 + 30) / 3 = 20.00
//	And so on.

	public static void main(final String[] args) {

		final int arr[] = { 39, 72, 44, 66, 57, 70, 63, 91, 70, 77, 12, 80, 56, 10, 80, 72, 37, 88, 73, 84, 61, 41, 57,
				26, 37, 7 };

		final float[] avg = new float[arr.length];
		float sum = 0;
		for (int i = 0; i < (arr.length); i++) {
			sum += arr[i];
			avg[i] = sum / (i + 1);

		}
		avg[avg.length - 1] = sum / (avg.length);
		for (final float element : avg) {
			System.out.println(element);
		}
	}

	float[] streamAvg(final int[] arr, final int n) {
		// code here
		final float[] avg = new float[arr.length];
		float sum = 0;
		for (int i = 0; i < (arr.length); i++) {
			sum += arr[i];
			avg[i] = sum / (i + 1);

		}
		avg[avg.length - 1] = sum / (avg.length);
		return avg;
	}

}
