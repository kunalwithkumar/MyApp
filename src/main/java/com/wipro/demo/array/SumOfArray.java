package com.wipro.demo.array;

public class SumOfArray {
//	Arrays (Sum of array)
//	SchoolAccuracy: 71.2%Submissions: 73828Points: 0
//	Given an array of N integers. Your task is to print the sum of all of the integers.
//	 
//
//	Example 1:
//
//	Input:
//	4
//	1 2 3 4
//	Output:
//	10
//	 
//
//	Example 2:
//
//	Input:
//	6
//	5 8 3 10 22 45
//	Output:
//	93

	public static void main(final String[] args) {

		final int arr[] = { 1, 2, 3, 4, 5 };
		long sum = 0;
		for (final int element : arr) {
			sum += (long)element;
		}
		System.out.println(sum);
	}

	public long getSum(final long a[], final long n) {
		// Your code goes here

		long sum = 0;
		for (int i = 0; i < n; i++) {
			sum += (long)a[i];
		}
		return sum;
	}

}
