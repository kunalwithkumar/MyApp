package com.wipro.demo.array;

public class PalindromArray {

//	Palindromic Array
//	SchoolAccuracy: 58.62%Submissions: 82061Points: 0
//	Given a Integer array A[] of n elements. Your task is to complete the function PalinArray. Which will return 1 if all the elements of the Array are palindrome otherwise it will return 0.
//
//	 
//
//	Example:
//	Input:
//	2
//	5
//	111 222 333 444 555
//	3
//	121 131 20
//
//	Output:
//	1
//	0
//
//	Explanation:
//	For First test case.
//	n=5;
//	A[0] = 111    //which is a palindrome number.
//	A[1] = 222   //which is a palindrome number.
//	A[2] = 333   //which is a palindrome number.
//	A[3] = 444  //which is a palindrome number.
//	A[4] = 555  //which is a palindrome number.
//	As all numbers are palindrome so This will return 1.
//
//	Constraints:
//	1 <=T<= 50
//	1 <=n<= 20
//	1 <=A[]<= 10000

	public static int palinArray(final int[] a, final int n) {
		// add code here.
		int flag = 1;

		for (int i = 0; i < n; i++) {
			final String temp = String.valueOf(a[i]);
			final String str = new StringBuilder(temp).reverse().toString();

			if (!temp.equals(str)) {
				flag = 0;
				break;
			}

		}
		if (flag == 0)
			return 0;
		return 1;

	}

}
