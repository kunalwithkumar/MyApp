package com.wipro.demo.array;

public class LargestNumber {

	public static void main(final String[] args) {

		final int arr[] = { 1, 2, 444, 5567, 6789 };
		int largest = Integer.MIN_VALUE;
		for (final int element : arr) {
			if (element > largest) {
				largest = element;
			}
		}
		System.out.println(largest);
	}

}
