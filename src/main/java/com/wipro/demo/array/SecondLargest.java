package com.wipro.demo.array;

public class SecondLargest {

//	Second Largest
//	SchoolAccuracy: 26.72%Submissions: 100k+Points: 0
//	Given an array Arr of size N, print second largest element from an array.
//
//	Example 1:
//
//	Input: 
//	N = 6
//	Arr[] = {12, 35, 1, 10, 34, 1}
//	Output: 34
//	Explanation: The largest element of the 
//	array is 35 and the second largest element
//	is 34.
//	Example 2:
//
//	Input: 
//	N = 3
//	Arr[] = {10, 5, 10}
//	Output: 5
//	Explanation: The largest element of 
//	the array is 10 and the second 
//	largest element is 5.
//	Your Task:
//	You don't need to read input or print anything. Your task is to complete the function print2largest() which takes the array of integers arr and n as parameters and returns an integer denoting the answer. If 2nd largest element doesn't exist then return -1.
//
//	Expected Time Complexity: O(N)
//	Expected Auxiliary Space: O(1)
//
//	Constraints:
//	1 ≤ N ≤ 105
//	1 ≤ Arri ≤ 105

	public static void main(final String[] args) {
		final int arr[] = { 1, 22, 345, 5677, 347 };

//		int largest, secondLargest =Integer.MIN_VALUE;
//		largest = Integer.MIN_VALUE;
//
//		Arrays.sort(arr);
//		largest = arr[arr.length - 1];
//		for (int i = arr.length - 1; i >= 0; --i) {
//			if (arr[i] != largest) {
//				secondLargest = arr[i];
//				break;
//			}
//			if(arr[i]==arr[arr.length-1])
//			secondLargest=-1;
//
//		}
//		System.out.println(secondLargest);

		int max = -1, secondMax;
		secondMax = max;
		for (final int element : arr) {
			if (element > max) {
				max = element;

			} else if ((element < max) && (element > secondMax)) {
				secondMax = element;
			}
		}
		System.out.println(secondMax);

	}

}
