package com.wipro.demo.inheritance;

public class Parent {

	private Integer id;
	private String name;

	public Parent() {
		// TODO Auto-generated constructor stub
	}

	public Parent(final Integer id, final String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

}
