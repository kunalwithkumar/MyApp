package com.wipro.demo;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class PermAdd implements Address {

	@Override
	public String city() {
		return "PermAdd";
	}

}
