package com.wipro.demo.service.impl;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.wipro.demo.entity.User;
import com.wipro.demo.repository.UserRepo;
import com.wipro.demo.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepo userRepo;

	@Override
	public List<User> getAllUsers() {
		return userRepo.findAll();
	}

	@Override
	public User addUser(final User user) {
		return userRepo.save(user);
	}

	@Override
	public User updateUser(final User user) {
		return userRepo.save(user);
	}

	@Override
	public ResponseEntity<User> removeUser(final Long id) {

		final User user = userRepo.findById(id).orElse(null);

		if (Objects.isNull(user))
			return new ResponseEntity<>(user, HttpStatus.NOT_FOUND);
		userRepo.delete(user);
		return new ResponseEntity<>(user, HttpStatus.GONE);
	}

	@Override
	public List<User> searchUsers(final String key) {
		return userRepo.searchUser(key);
	}

	@Override
	public User findUserById(final Long id) {
		final User user = userRepo.findById(id).orElse(null);
		if (Objects.isNull(user)) {
		}
		return user;

	}

}
