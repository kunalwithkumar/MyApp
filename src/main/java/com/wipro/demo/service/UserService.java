package com.wipro.demo.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.wipro.demo.entity.User;

public interface UserService {

	public List<User> getAllUsers();

	public User addUser(User user);

	public User updateUser(User user);

	public ResponseEntity<User> removeUser(Long id);

	public List<User> searchUsers(String key);

	public User findUserById(Long id);
	

}
