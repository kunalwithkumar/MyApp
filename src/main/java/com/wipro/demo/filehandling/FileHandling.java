package com.wipro.demo.filehandling;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileHandling {

//	public static void main(final String[] args) {
//
//		try {
//			final File file = new File(
//					"/Users/KU20423438/Documents/workspace-spring-tool-suite-4-4.16.0.RELEASE/demo/src/main/java/com/wipro/demo/filehandling/file.txt");
//			if (!file.exists())
//				throw new IOException();
//			final PrintWriter writer = new PrintWriter(file);
//			writer.write("we are writing from program");
//			writer.close();
//			final Scanner scanner = new Scanner(file);
//			while (scanner.hasNextLine()) {
//				System.out.println(scanner.nextLine());
//			}
//			scanner.close();
//
//		} catch (final IOException e) {
//			e.printStackTrace();
//		}
//
//	}

//	public static void main(final String[] args) throws FileNotFoundException {
//
//		final File file = new File(
//				"/Users/KU20423438/Documents/workspace-spring-tool-suite-4-4.16.0.RELEASE/demo/src/main/java/com/wipro/demo/filehandling/file.txt");
//		FileReader fileReader= new FileReader(file);
//		BufferedReader bufferedReader= new BufferedReader(fileReader);
//		
//		
//
//	}

	public static void main(final String[] args) {

		final File file = new File(
				"/Users/KU20423438/Documents/workspace-spring-tool-suite-4-4.16.0.RELEASE/demo/src/main/java/com/wipro/demo/filehandling/file.txt");

		// because of AutoClosable interface we can use try with resources 1.7
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(file));) {
			if (file.exists()) {
				writer.write("Hello File Writting from Program 3  line 1");
				writer.write("\n");
				writer.write("Hello File Writting from Program 3 ");
				writer.write("\n");
				writer.write("Hello File Writting from Program 3 ");
				writer.write("\n");
				writer.write("Hello File Writting from Program 3 last line");

			}
		} catch (final IOException e) {
			e.printStackTrace();
		}
		try (BufferedReader reader = new BufferedReader(new FileReader(file));) {
			reader.lines().forEach(word -> {
				System.out.println(word);
			});
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

}
