package com.wipro.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.demo.Address;
import com.wipro.demo.entity.User;
import com.wipro.demo.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private Address address;

	@GetMapping("/address")
	public String getAdd() {
		return address.city();
	}

	@GetMapping("/get")
	public String getString() {
		return "Hello";
	}

	@GetMapping("/getUser")
	public List<User> getAllUsers() {
		return userService.getAllUsers();

	}

	@PostMapping("/add")
	public User addUser(@RequestBody final User user) {
		return userService.addUser(user);
	}

	@PutMapping("/update")
	public User updateUser(@RequestBody final User user) {
		return userService.updateUser(user);
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<User> removeUser(@PathVariable final Long id) {
		return userService.removeUser(id);
	}

	@GetMapping("/search/{key}")
	public List<User> searchUsers(@PathVariable final String key) {
		return userService.searchUsers(key);
	}

	@GetMapping("/get/{id}")
	public User findUserById(@PathVariable final Long id) {
		return userService.findUserById(id);
	}

}
