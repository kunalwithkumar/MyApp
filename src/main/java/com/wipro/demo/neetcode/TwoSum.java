package com.wipro.demo.neetcode;

import java.util.Arrays;
import java.util.HashMap;

public class TwoSum {

//	Problem Statement
//	​
//	This is another article in the series leetcode problem solutions and this article is a solution to leetcode 1 two sum problem.
//
//
//	Once you have a good understanding of two sum problem, it should help you solve advanced level problems like three sum which in some ways a continuation of the two sum problem.
//
//
//	Consider you are given an array of integers and a target sum, return indices of two numbers in the array such that they add  up to the given target. You may assume that each input would have exactly one solution. Also, you cannot use the same element twice. You are allowed to return the  answer in any order.
//
//
//	Example
//	​
//	Example 1:
//
//	Input: nums = [7,2,13,11], target = 9
//	Output: [0,1]
//	Example 2:
//
//	Input: nums = [7,3,5], target = 8
//	Output: [1,2]

	public static void main(final String[] args) {

		final int nums[] = { 5, 2, 10, 5 };
		final int sum = 10;
		final int arr[] = new int[2];
		Arrays.sort(nums);
		for (int i = 0; i < nums.length; i++) {
			for (int j = 1; j < nums.length; j++) {
				if (((nums[i] + nums[j]) == sum) && (i != j)) {
					arr[0] = i;
					arr[1] = j;
					break;
				}

			}
		}
		System.out.println(arr[0] + " " + arr[1]);

	}

	public int[] twoSum(final int[] nums, final int target) {
		final int arr[] = new int[2];
		Arrays.sort(arr);
		for (int i = 0; i < nums.length; i++) {
			for (int j = 1; j < nums.length; j++) {
				if (((nums[i] + nums[j]) == target) && (i != j)) {
					arr[0] = i;
					arr[1] = j;
					break;
				}

			}
		}
		return arr;

	}

	public int[] twoSum2(final int[] numbers, final int target) {

		final HashMap<Integer, Integer> indexMap = new HashMap<>();
		for (int i = 0; i < numbers.length; i++) {
			final Integer requiredNum = target - numbers[i];
			if (indexMap.containsKey(requiredNum)) {
				return new int[] { indexMap.get(requiredNum), i };
			}

			indexMap.put(numbers[i], i);
		}
		return null;
	}
}
