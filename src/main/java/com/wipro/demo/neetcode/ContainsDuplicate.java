package com.wipro.demo.neetcode;

import java.util.Arrays;

public class ContainsDuplicate {

	public static void main(final String[] args) {

		final int arr[] = { 1, 2, 3, 4, 1 };
		Arrays.sort(arr);
		boolean val = false;
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] == arr[i - 1]) {
				val = true;
				break;
			}
		}
		System.out.println(val);

	}

}
