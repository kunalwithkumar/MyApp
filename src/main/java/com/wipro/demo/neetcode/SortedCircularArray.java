package com.wipro.demo.neetcode;

import java.util.Arrays;

public class SortedCircularArray {
	public static void main(final String[] args) {
		final int arr[] = { 3, 4, 5, 1, 2 };

		Arrays.sort(arr);
		for (final int element : arr) {
			System.out.print(element + " ");
		}

	}

}
