package com.wipro.demo.neetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DisappearedNumber {

//	448. Find All Numbers Disappeared in an Array
//	Easy
//
//	7630
//
//	412
//
//	Add to List
//
//	Share
//	Given an array nums of n integers where nums[i] is in the range [1, n], 
	// return an array of all the integers in the range [1, n] that do not appear in
	// nums.
//
//	 
//
//	Example 1:
//
//	Input: nums = [4,3,2,7,8,2,3,1]
//	Output: [5,6]
//	Example 2:
//
//	Input: nums = [1,1]
//	Output: [2]
//	 
//
//	Constraints:
//
//	n == nums.length
//	1 <= n <= 105
//	1 <= nums[i] <= n

	public static void main(final String[] args) {

		final int nums[] = { 1, 1 };
		final int n = nums.length;
		// System.out.println(n);
		Arrays.sort(nums);
		final List<Integer> list = new ArrayList<>();
		for (int i = 1; i < (n + 1); i++) {
			// System.out.println(nums[i] + " num is");
			// System.out.println(i);

			// System.out.println(Arrays.binarySearch(nums, i));
			if (Arrays.binarySearch(nums, i) < 0) {
				list.add(i);
				// System.out.println("inside");
			}
		}
		System.out.println(list);

	}

}
