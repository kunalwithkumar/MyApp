package com.wipro.demo.neetcode;

import java.util.Arrays;

public class ValidAnagram {

//	242. Valid Anagram
//	Easy
//
//	7497
//
//	247
//
//	Add to List
//
//	Share
//	Given two strings s and t, return true if t is an anagram of s, and false otherwise.
//
//	An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.
//
//	 
//
//	Example 1:
//
//	Input: s = "anagram", t = "nagaram"
//	Output: true
//	Example 2:
//
//	Input: s = "rat", t = "car"
//	Output: false
//	 
//
//	Constraints:
//
//	1 <= s.length, t.length <= 5 * 104
//	s and t consist of lowercase English letters.

	public static void main(final String[] args) {
		// TODO Auto-generated method stub

		final String s = "abchdjdn";
		final String t = "bca";
		boolean flag = false;
		final char ss[] = s.toCharArray();
		final char tt[] = t.toCharArray();

		Arrays.sort(ss);
		Arrays.sort(tt);

		if (s.length() == t.length()) {
			flag = Arrays.equals(ss, tt);
		}
		System.out.println(flag);

	}

}
