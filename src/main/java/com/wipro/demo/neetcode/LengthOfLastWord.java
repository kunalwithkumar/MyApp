package com.wipro.demo.neetcode;

public class LengthOfLastWord {

//	58. Length of Last Word
//	Easy
//
//	2127
//
//	130
//
//	Add to List
//
//	Share
//	Given a string s consisting of words and spaces, return the length of the last word in the string.
//
//	A word is a maximal substring consisting of non-space characters only.
//
//	 
//
//	Example 1:
//
//	Input: s = "Hello World"
//	Output: 5
//	Explanation: The last word is "World" with length 5.
//	Example 2:
//
//	Input: s = "   fly me   to   the moon  "
//	Output: 4
//	Explanation: The last word is "moon" with length 4.
//	Example 3:
//
//	Input: s = "luffy is still joyboy"
//	Output: 6
//	Explanation: The last word is "joyboy" with length 6.
//	 
//
//	Constraints:
//
//	1 <= s.length <= 104
//	s consists of only English letters and spaces ' '.
//	There will be at least one word in s.

	public static void main(final String[] args) {

		final String s = "luffy is still joyboy";

		final String[] arrOfStr = s.split(" ");
		final int count = arrOfStr[arrOfStr.length - 1].length();

		// max length in array of string
		// count=0;
//		for (final String element : arrOfStr) {
//			// System.out.println(element);
//			if (element.length() > count) {
//				count = element.length();
//			}
//		}
		System.out.println(count);

	}

	public int lengthOfLastWord(final String s) {

		final String[] arrOfStr = s.split(" ");
		return arrOfStr[arrOfStr.length - 1].length();
	}

}
