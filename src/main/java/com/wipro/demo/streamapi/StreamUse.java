package com.wipro.demo.streamapi;

import java.util.ArrayList;
import java.util.List;

public class StreamUse {

	public static void main(final String[] args) {

		final List<Integer> list = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			list.add(i);
		}
//		list.stream().forEach(i -> {
//			System.out.println(i);
//		});

		// System.out.println(list.stream().count());
//		list.stream().filter(i -> i > 4).forEach(i -> {
//			System.out.println(i);
//		});

//		list.stream().map(x -> x * 2).forEach(i -> {
//			System.out.println(i);
//		});

//		final List<Integer> integers = list.stream().distinct().collect(Collectors.toList());
//		System.out.println(integers);

//		list.stream().filter(i -> (i % 2) == 0).forEach(i -> {
//			System.out.println(i);
//		});

//		list.stream().map(i -> i * 3).forEach(i -> {
//			System.out.println(i);
//		});

		final int even = list.stream().filter(x -> (x % 2) == 0).reduce(0, (ans, i) -> ans + i);
		System.out.println(even);

	}

}
