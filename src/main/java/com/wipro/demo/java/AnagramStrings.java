package com.wipro.demo.java;

import java.util.Arrays;

public class AnagramStrings {

//	Anagram Strings
//	BasicAccuracy: 46.34%Submissions: 5045Points: 1
//	Given two strings S1 and S2 . Print "1" if both strings are anagrams otherwise print "0" .
//
//	Note: An anagram of a string is another string with exactly the same quantity of each character in it, in any order.
//
//	Example 1:
//
//	Input: S1 = "cdbkdub" , S2 = "dsbkcsdn"
//	Output: 0 
//	Explanation: Length of S1 is not same
//	as length of S2.
//	Example 2:
//
//	Input: S1 = "geeks" , S2 = "skgee"
//	Output: 1
//	Explanation: S1 has the same quantity 
//	of each character in it as S2.
//
//	Your Task:  
//	You don't need to read input or print anything. Your task is to complete the function areAnagram() which takes S1 and S2 as input and returns "1" if both strings are anagrams otherwise returns "0".
//
//	Expected Time Complexity: O(n)
//	Expected Auxiliary Space: O(K) ,Where K= Contstant
//
//	Constraints:
//	1 <= |S1| <= 1000
//	1 <= |S2| <= 1000 

	public static void main(final String[] args) {
		// TODO Auto-generated method stub
		System.out.println(areAnagram("cdbkdub", "dsbkcsdn"));

	}

	static int areAnagram(final String S1, final String S2) {

		if (S1.length() != S2.length())
			return 0;
		// code here
		final char ch1[] = S1.toCharArray();
		final char ch2[] = S2.toCharArray();
		Arrays.sort(ch1);
		Arrays.sort(ch2);
		for (int i = 0; i < ch1.length; i++) {
			if (ch1[i] != ch2[i])
				return 0;

		}

		return 1;
	}

}
