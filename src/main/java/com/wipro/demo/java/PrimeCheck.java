package com.wipro.demo.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class PrimeCheck {

	public static void main(final String[] args) {
		// dataTypes(1, 2, 3, 5, (byte) 10);

	}

	public static int unique_substring(final String str) {
		// Your code here

		final Set<String> s = new HashSet<>();

		// List All Substrings
		for (int i = 0; i <= str.length(); i++) {
			for (int j = i + 1; j <= str.length(); j++) {

				// Add each substring in Set
				s.add(str.substring(i, j));
			}
		}
		return s.size();

	}

	void insert(final LinkedHashSet<Integer> s, final int x) {
		// Your code here
		s.add(x);
	}

	/* prints the contents of the set s in ascending order */
	void print_Contents_Increasing_Order(final LinkedHashSet<Integer> s) {
		// Your code here
		s.stream().sorted().forEach(stm1 -> System.out.println(stm1));

	}

	/* prints the contents of the set s in ascending order */
	void print_Contents_Insertion_Order(final LinkedHashSet<Integer> s) {
		// Your code here
		for (final int i : s) {
			System.out.print(i + " ");
		}

	}

	/* erases an element x from the set s */
	void erase(final LinkedHashSet<Integer> s, final int x) {
		// Your code here
		s.remove(x);
	}

	/* returns the size of the set s */
	int size(final LinkedHashSet<Integer> s) {
		// Your code here
		return s.size();
	}

	/*
	 * returns 1 if the element x is present in set s else returns -1
	 */
	int find(final LinkedHashSet<Integer> s, final int x) {
		// Your code here
		if (s.contains(x))
			return 1;
		return -1;
	}

//	public static final int END = Integer.MAX_VALUE;
//	public static final int START = END - 100;
//
//	public static void main(final String[] args) {
//		int count = 0;
//		//for (int i = START; i <= END; i++) {
//			//System.out.println(i);
//			count++;
//		//}
//		System.out.println(count);
//	}

	public static ArrayList<Integer> common_element(final int v1[], final int v2[]) {
		// Your code here
		final ArrayList<Integer> list = new ArrayList<>();

		for (final int element : v1) {
			for (int j = 0; j < v2.length; j++) {
				// if(v1)
				list.add(element);
			}
		}
		Collections.sort(list);
		return list;
	}

	static void dataTypes(final int a, final float b, final double c, final long l, final byte d) {

		final double p = c / b;// c/b
		final double q = b / a;// b/a
		final double r = c / a;// c/a
		final double m = r + l;// r+l
		final int s = a / d;// a/d

		// Printing all the results
		System.out.println(p + " " + q + " " + r + " " + m + " " + s);

	}

	public static boolean isPrime(final int n) {
		if (n <= 1)
			return false;
		if (n == 2)
			return true;
		for (int i = 2; i <= Math.sqrt(n); i++) {
			if ((n % i) == 0)
				return false;
		}

		return true;
	}

	static public void display(final int k) {
		int c = 0;
		// Add your code here.
		for (int i = 2; i <= k; i++) {
			if (isPrime(i)) {
				c++;
			}
		}
		System.out.println(c);
	}

	static void printInFormat(final float a, final float b) {
		final float result = a / b;

		// Your code here to print upto 3 decimal places
		System.out.format(result + " ");
		System.out.format("%.3f", result);

	}

	static void getInput() {

		final Scanner sc = new Scanner(System.in);
		int t = sc.nextInt(); // Taking the number of testcases
		while (t-- > 0) {
			final int a = sc.nextInt();
			String s = sc.next();
			s += sc.nextLine();

			// Your code here

			System.out.println(a);
			System.out.println(s);
		}
		sc.close();

	}

}

class GenericClass<T> {

	// Add your code here. Make a private data variable, constructor which intialize
	// the data variable and a method showType().
	private final T t;

	GenericClass(final T t) {
		this.t = t;
	}

	void showType() {
		System.out.println(t.getClass().getSimpleName());
		System.out.println(t);

	}

}

class Pair {
	int x, y;
}

class custom_Compare {

	// Compare function
	static void sortPairs(final Pair arr[], final int N) {

		// Your code here

		Arrays.sort(arr, new Sortbyroll());
		// printing the x,y Pairs
		for (int i = 0; i < N; i++) {
			System.out.print(arr[i].x + " " + arr[i].y + " ");
		}
		System.out.println();
	}
}

class Sortbyroll implements Comparator<Pair> {
	// Used for sorting in ascending order of
	// roll number
	@Override
	public int compare(final Pair a, final Pair b) {
		return a.x - b.x;
	}
}
