package com.wipro.demo.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DishProblem {

	public static void main(final String[] args) {

		final int arr[] = { -1, -9, 0, 5, -7 };

		final List<Integer> arr2 = new ArrayList<>();

		arr2.add(arr[0]);
		Arrays.sort(arr);

		for (final int element : arr) {
			if (element > (-1)) {
				arr2.add(element);
			}
		}

		int sum = 0;
		for (int i = 1; i <= arr2.size(); i++) {
			final int val = i * arr2.get(i - 1);
			sum += val;
		}
		System.out.println("value is " + sum);
	}

}
