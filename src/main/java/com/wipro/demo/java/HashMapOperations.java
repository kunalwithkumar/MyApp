package com.wipro.demo.java;

import java.util.HashMap;
import java.util.Objects;

public class HashMapOperations {

//	HashMap Operations
//	BasicAccuracy: 56.28%Submissions: 4210Points: 1
//	Implement different operations on Hashmap. Different types of queries will be provided.
//	A query can be of four types:
//	1. a x y (adds an entry with key x and value y to the Hashmap)
//	2. b x (print value of  x if present in the Hashmap else print -1. )
//	3. c (prints the size of the Hashmap)
//	4. d x (removes an entry with key x from the Hashmap)
//
//	Example 1 :
//
//	Input:
//	5 
//	a 1 2 a 66 3 b 66 d 1 c 
//
//	Output:
//	3 1 
//
//	Explanation :
//	There are five queries. Queries are performed in this order
//	1. a 1 2  --->  map has a key 1 with value 2
//	2. a 66 3 ---> map has a key 66 with value 3
//	3. b 66   ---> prints the value of key 66 if its present in the map ie 3.
//	4. d 1    ---> removes an entry from map with key 1
//	5. c      ---> prints the size of the map ie 1
//	Example 2 :
//
//	Input: 
//	3 
//	a 1 66 b 5 c
//
//	Output: 
//	-1 1
//
//	Explanation :
//	There are three queries. Queries are performed in this order
//	1. a 1 66 ---> adds a key 1 with a value of 66 in the map
//	2. b 5    --->  since the key 5 is not present in the map hence -1 is printed.
//	3. c      ---> prints the size of the map ie 1
//	Your Task:
//	You are required to complete the following functions:
//	add_Value : Takes HashMap, x, y as arguments and maps x as key and y as its value. Does not return anything.
//	find_value : Takes HashMap and x as arguments. If HM contains x key then return the value, else return -1.
//	getSize : Takes HashMap as argument and just returns its size.
//	removeKey : Takes HashMap and x as arguments and removes x if it exists. Does not return anything.
//
//	Constraints:
//	1 <= Q <= 100
//

	public static void main(final String[] args) {
		// TODO Auto-generated method stub
	}

	/* Inserts an entry with key x and value y in map */
	void add_Value(final HashMap<Integer, Integer> hm, final int x, final int y) {
		// Your code here
		hm.put(x, y);
	}

	/* Returns the value with key x from the map */
	int find_value(final HashMap<Integer, Integer> hm, final int x) {
		// Your code here

		final Integer n = hm.get(x);
		if (Objects.isNull(n))
			return -1;
		return n;
	}

	/* Returns the size of the map */
	int getSize(final HashMap<Integer, Integer> hm) {
		// Your code here
		return hm.size();
	}

	/* Removes the entry with key x from the map */
	void removeKey(final HashMap<Integer, Integer> hm, final int x) {
		// Your code here
		hm.remove(x);
	}

}
