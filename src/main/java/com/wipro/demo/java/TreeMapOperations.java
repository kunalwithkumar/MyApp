package com.wipro.demo.java;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.TreeMap;

public class TreeMapOperations {

	public static void main(final String[] args) {
		// TODO Auto-generated method stub

		final LinkedHashSet<Integer> hashSet = new LinkedHashSet<>();
		hashSet.add(77);

		hashSet.stream().sorted().forEach(stm1 -> System.out.println(stm1));
	}

	void add_Value(final TreeMap<Integer, Integer> hm, final int x, final int y) {
		// Your code here
		hm.put(x, y);
	}

	/* Returns the value with key x from the map */
	int find_value(final TreeMap<Integer, Integer> hm, final int x) {
		// Your code here
		final Integer i = hm.get(x);
		if (Objects.isNull(i))
			return -1;
		return i;
	}

	/* Returns the size of the map */
	int getSize(final TreeMap<Integer, Integer> hm) {
		// Your code here
		return hm.size();
	}

	/* Removes the entry with key x from the map */
	void removeKey(final TreeMap<Integer, Integer> hm, final int x) {
		// Your code here
		hm.remove(x);
	}

	/* print map sorted by key */
	void sorted_By_Key(final TreeMap<Integer, Integer> hm) {
		// Your code here
		System.out.println(hm.keySet().toString());
	}

}
