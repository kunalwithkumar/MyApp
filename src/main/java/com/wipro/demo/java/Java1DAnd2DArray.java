package com.wipro.demo.java;

import java.util.ArrayList;

public class Java1DAnd2DArray {

//	Java 1-d and 2-d Array
//	BasicAccuracy: 57.44%Submissions: 17976Points: 1
//	Given a integer n. We have n*n values of a 2-d array, and  n values of 1-d array. 
	// Task is to find the sum of the left diagonal values of the 2-d array and
	// the max element of the 1-d array and print them with space in between.
//
//	Example 1:
//
//	â€‹Input : arr[][] = {{1,2,3}, {4,5,6}, {7, 8,9}} 
//	        and N = 3
//	brr[] = {3, 6, 9}
//	Output : 15 9
//	Explanation:
//	1 2 3
//	4 5 6
//	7 8 9
//	So, this sum of left diagonal (1+ 5 + 9) = 15
//	The maximum element in an array brr is 9
//	So, will return {15, 9} as an answer.
//
//	â€‹Example 2:
//
//	Input : arr[][] = {{1,2}, {1, 2}} and N = 2
//	brr[] = {10, 1} 
//	Output :  3 10 
//	 
//
//	Your Task:
//	This is a function problem. The input is already taken care of by the driver code. 
	// You only need to complete the function array() that takes a two-dimension
	// array (a),
	// another one dimension array (b), sizeOfArray (n), and return
	// the ArrayList which is having the sum of the diagonal elements of the array a
	// and
	// the maximum number of the array b. The driver code takes care of the
	// printing.
//
//	Expected Time Complexity: O(N2).
//	Expected Auxiliary Space: O(1).

	public static void main(final String[] args) {
		final int arr[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		final int brr[] = { 3, 6, 9 };
		final int n = 3;
		System.out.println(array(arr, brr, n));

	}

	public static ArrayList<Integer> array(final int a[][], final int b[], final int n) {
		int sum = 0;

		for (int i = 0, j = 0; (i < n) && (j < n); i++, j++) {
			if (i == j) {
				sum += a[i][j];
			}

		}
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < n; i++) {
			if (b[i] > max) {
				max = b[i];
			}
		}
		final ArrayList<Integer> list = new ArrayList<>();
		list.add(sum);
		list.add(max);
		return list;

	}

}
