package com.wipro.demo.java;

public class Factorial {

	public static void main(final String[] args) {
		System.out.println(factorial(5L));

		// java 14 onwards
//		String day = "TH";
//		String result = switch (day) {
//		    case "M", "W", "F" -> "MWF";
//		    case "T", "TH", "S" -> "TTS";
//
//		    default -> {
//			    if (day.isEmpty())
//				    yield "Please insert a valid day.";
//			    else
//				    yield "Looks like a Sunday.";
//		    }
//		};

		// System.out.println(result);

//		int choice = 2;
//
//		int x = switch (choice) {
//		    case 1, 2, 3:
//			    yield choice;
//		    default:
//			    yield -1;
//		};
//
//		System.out.println("x = " + x); // x = 2

	}

	public static long factorial(final long n) {
		if (n == 1)
			return 1;
		return (n * factorial(n - 1));
	}

}
