package com.wipro.demo.java;

public class Palindrome {

	public static void main(final String[] args) {

		System.out.println(checkPalindromeString("abba"));
	}

	static boolean checkPalindromeString(final String input) {
		boolean result = true;
		final int length = input.length();

		for (int i = 0; i < (length / 2); i++) {
			if (input.charAt(i) != input.charAt(length - i - 1)) {
				result = false;
				break;
			}
		}

		return result;
	}

}
