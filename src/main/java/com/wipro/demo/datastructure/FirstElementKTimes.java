package com.wipro.demo.datastructure;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class FirstElementKTimes {

	public static void main(final String[] args) {

		// System.out.println(firstElementKTime(arr, arr.length, 3));
		System.out.println(isAnagram("hello", "hello"));
	}

	public static boolean isAnagram(final String a, final String b) {

		// Your code here
		final char arr1[] = a.toCharArray();
		final char arr2[] = b.toCharArray();
		if (arr1.length != arr2.length)
			return false;
		Arrays.sort(arr1);
		Arrays.sort(arr2);

		for (int i = 0; i < arr2.length; i++) {
			if (arr1[i] != arr2[i])
				return false;
		}

		return true;

	}

	static public int firstElementKTime(final int[] a, final int n, final int k) {
		final HashMap<Integer, Integer> map = new HashMap<>();

		for (int i = 0; i < n; i++) {
			if (map.containsKey(a[i])) {

				final int c = map.get(a[i]);
				System.out.println(c + "c");
				map.replace(a[i], c + 1);

			} else {
				map.put(a[i], 1);
			}

//			for (final Map.Entry<Integer, Integer> entry : map.entrySet()) {
//				if (entry.getValue() == k)
//					return entry.getKey();
//			}

		}
		// System.out.println(map);

		return -1;
	}

	static int getOddOccurrence(final int[] arr, final int n) {
		final HashMap<Integer, Integer> map = new HashMap<>();

		// code here

		for (int i = 0; i < n; i++) {
			if (map.containsKey(arr[i])) {

				final int c = map.get(arr[i]);
				map.replace(arr[i], c + 1);
			} else {
				map.put(arr[i], 1);
			}

		}
		System.out.println(map);
		for (final Map.Entry<Integer, Integer> entry : map.entrySet()) {
			if ((entry.getValue() % 2) != 0)
				return entry.getKey();
		}
		return -1;
	}

}
