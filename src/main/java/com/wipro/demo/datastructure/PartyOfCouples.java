package com.wipro.demo.datastructure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PartyOfCouples {

//	Party of Couples
//	SchoolAccuracy: 50.5%Submissions: 31K+Points: 0
//	Lamp
//	This problem is part of GFG SDE Sheet. Click here to view more.  
//
//	In a party of N people, each person is denoted by an integer. Couples are represented by the same number. Find out the only single person in the party of couples.
//
//
//	Example 1:
//
//	Input: N = 5
//	arr = {1, 2, 3, 2, 1}
//	Output: 3
//	Explaination: Only the number 3 is single.
//
//	Example 2:
//
//	Input: N = 11
//	arr = {1, 2, 3, 5, 3, 2, 1, 4, 5, 6, 6}
//	Output: 4
//	Explaination: 4 is the only single.
//
//	Your Task:
//	You do not need to read input or print anything. Your task is to complete the function findSingle() which takes the size of the array N and the array arr[] as input parameters and returns the only single person.
//
//
//	Expected Time Complexity: O(N)
//	Expected Auxiliary Space: O(1)
//
//
//	Constraints:
//	1 ≤ N ≤ 104
//	1 ≤ arr[i] ≤ 106

	public static void main(final String[] args) {

		// splitString("geeks2345!@#$dfgh456");

		logicalOperation(1, 1, 1, 1, 1, 1);

//		final String s = "Geeks";
//		final char chh[] = s.toCharArray();
//		final ArrayList<String> arrayList = new ArrayList<>();
//		arrayList.add(s);
//
//		for (int i = chh.length - 1; i > 0; i--) {
//			chh[i] = ' ';
//			String s2 = String.valueOf(chh);
//			s2 = s2.strip();
//
//			arrayList.add(s2);
//
//		}
//		System.out.println(arrayList);

//		final int matrix[][] = { { 10, 28, 35, 36, 17, 15, 15, 18 }, { 20, 15, 28, 18, 40, 45, 36, 41 },
//				{ 6, 28, 46, 20, 19, 41, 46, 15 }, { 14, 25, 22, 6, 30, 19, 4, 21 }, { 21, 19, 20, 7, 17, 32, 8, 34 },
//				{ 16, 39, 29, 29, 31, 48, 7, 35 }, { 36, 38, 33, 37, 7, 11, 44, 33 },
//				{ 16, 5, 10, 22, 6, 42, 17, 15 } };
//
//		System.out.println(DiagonalSum(matrix));

//		final int mat[][] = { { 11, 74 }, { 0, 93 }, { 40, 11 }, { 74, 7 } };
//
//		final int n = 2, m = 4;
//
//		System.out.println(isToepliz(mat, n, m));

//		final int n = 5;
//		System.out.println(Integer.toBinaryString(n));

//		final int arr[] = { 1, 1, 2, 3, 44, 2, 3 };
//		System.out.println(findSingle(arr.length, arr));

	}

	static List<String> splitString(final String S) {
		final List<String> strings = new ArrayList<>();
		final StringBuilder s1 = new StringBuilder();
		final StringBuilder s2 = new StringBuilder();
		final StringBuilder s3 = new StringBuilder();

		for (int i = 0; i < S.length(); i++) {
			final char ch = S.charAt(i);
			System.out.println(ch);
			if (((ch >= 'A') && (ch <= 'Z')) || ((ch >= 'a') && (ch <= 'z'))) {
				// System.out.println(ch);
				s1.append(ch);
			} else if ((ch >= '0') && (ch <= '9')) {
				System.out.println(ch);

				s2.append(ch);
			} else {
				System.out.println(ch);

				s3.append(ch);
			}
		}
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);
		if (s1.length() > 0) {
			strings.add(s1.toString());
		} else {
			strings.add("-1");
		}
		if (s2.length() > 0) {
			strings.add(s2.toString());
		} else {
			strings.add("-1");
		}
		if (s3.length() > 0) {
			strings.add(s3.toString());
		} else {
			strings.add("-1");
		}
		return strings;
	}

	public String transform(final String s) {
		final StringBuilder l = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			if ((i == 0) || (s.charAt(i - 1) == ' ')) {
				l.append((char) (s.charAt(i) - 32));
			} else {
				l.append(s.charAt(i));
			}

		}
		return l.toString();

	}

	public static void multiply(final int A[][], final int B[][], final int C[][], final int N) {
		// add code here.
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				for (int k = 0; k < N; k++) {
					C[i][j] += A[i][k] * B[k][j];

				}
			}
		}

	}

	static String reciprocalString(final String S) {

		final StringBuilder str = new StringBuilder();

		for (int i = 0; i < S.length(); i++) {
			char ch = S.charAt(i);
			if (Character.isLetter(ch)) {

				if (Character.isLowerCase(ch)) {
					ch = (char) ((122 - (ch)) + 97);
				}

				else if (Character.isUpperCase(ch)) {
					ch = (char) ((90 - (ch)) + 65);
				}
			}
			str.append(ch);
		}
		return str.toString();

	}

	static int logicalOperation(final int A, final int B, final int C, final int D, final int E, final int F) {
		return ((~A) & B) | (C & D) | (E & ~F);
	}

	String removeCharacters(final String S) {
		// code here
		final StringBuilder str = new StringBuilder();
		for (int i = 0; i < S.length(); i++) {
			final char ch = S.charAt(i);
			if (Character.isDigit(ch)) {
				str.append(ch);
			}
		}
		return str.toString();
	}

	String removeVowels(final String S) {
		// code here
		final StringBuilder str = new StringBuilder();
		for (int i = 0; i < S.length(); i++) {
			final char ch = S.charAt(i);
			if (((ch != 'a') && (ch != 'e') && (ch != 'i') && (ch != 'o') && (ch != 'u'))) {
				str.append(ch);
			}
		}
		return str.toString();
	}

	static String triDownwards(String s) {
		// code here

		final char arr[] = s.toCharArray();
		for (int i = 0; i < (arr.length - 1); i++) {
			arr[i] = '.';
			final String s2 = String.valueOf(arr);
			s += s2;
		}
		return s;
	}

	static int findSingle(final int N, final int arr[]) {

//		Arrays.sort(arr);
//	       for(int i=0;i<N-1;i=i+2)
//	       {
//	           if(arr[i]!=arr[i+1])
//	           return arr[i];
//	       }
//	       return arr[N-1];

		final Map<Integer, Integer> map = new HashMap<>();
		final int c = 1;
		for (final int i : arr) {
			if (map.containsKey(i)) {
				map.put(i, c + 1);
			} else {
				map.put(i, c);
			}

		}

		System.out.println(map);
		for (final Map.Entry<Integer, Integer> entry : map.entrySet()) {
			if (entry.getValue() == 1)
				return entry.getKey();

		}

		return 0;

	}

	public static int DiagonalSum(final int[][] matrix) {
		// code here
		int sum1 = 0, sum2 = 0;

		final int len = matrix[0].length;
		for (int i = 0; i < len; i++) {
			for (int j = 0; j < len; j++) {
				if ((i == j)) {
					// System.out.println(matrix[i][j]);
					sum1 += matrix[i][j];
					// System.out.println(sum);
				}
				if ((i + j) == (len - 1)) {
					sum2 += matrix[i][j];
				}
				// if(i)

			}
		}
		// System.out.println(sum);
		return sum1 + sum2;

//		 for(int i=0; i<N; i++)
//	        {
//	              // Since for primary diagonal sum the value of
//	            // row and column are equal
//	              Pd += matrix[i][i];
//	           
//	            // For secondary diagonal sum values of i'th index
//	            // and j'th index sum is equal to n-1 at each
//	            // stage of matrix
//	              Sd += matrix[i][N-(i+1)];
//	        }

	}

	static boolean isToepliz(final int mat[][], final int N, final int M) {
		// Your code here
		// if(N!=M)
		// return false;
//		final int dia = mat[0][0];
//		for (int i = 0; i < N; i++) {
//			for (int j = 0; j < M; j++) {
//				if (i == j)
//					if (dia != mat[i][j])
//						return false;
//			}
//		}
//		return true;
//		
		for (int i = 1; i < N; i++) {
			for (int j = 1; j < M; j++) {
				if (mat[i][j] == mat[i - 1][j - 1]) {
					continue;
				}
				return false;
			}
		}
		return true;
	}

	static String getBinaryRep(final int n) {
		int i;
		final StringBuilder s = new StringBuilder();
		for (i = 1 << 29; i > 0; i = i / 2) {
			if ((n & i) != 0) {
				s.append("1");
			} else {
				s.append("0");
			}
		}
		return s.toString();
	}
}
