package com.wipro.demo.datastructure;

public class ReverseString {

	public static void main(final String[] args) {
		// TODO Auto-generated method stub
		reverseWord("helloo");

	}

	public static String reverseWord(final String str) {

		return new StringBuilder(str).reverse().toString();
	}

}
