package com.wipro.demo.thread;

public class SyncCheck {

	public static void main(final String[] args) {

		final SyncObject object = new SyncObject();

		final Thread thread = new MyThreadExample(object);
		thread.start();

		final Thread thread2 = new MyThreadExample(object);
		thread2.start();

	}

}

class MyThreadExample extends Thread {

	SyncObject object = null;

	public MyThreadExample(final SyncObject object) {

		this.object = object;
	}

	@Override
	public void run() {

		for (int i = 0; i < 50; i++) {

			try {

				System.out.println(Thread.currentThread().getName() + " val ==> " + object.incVal());
				Thread.sleep(500);

			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
