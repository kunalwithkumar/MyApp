package com.wipro.demo.thread;

public class ThreadPractice {

	public static void main(final String[] args) throws InterruptedException {

		// new way
		final Thread thread = new Thread(() -> {
			Thread.currentThread().setName("MyThread");
			System.out.println("HEllo" + Thread.currentThread().getName());
		});
		thread.start();

		System.out.println(thread.getName() + " thread");
		System.out.println(thread.getPriority());
		System.out.println(thread.getState() + " calling thread getState()");
		// thread.yield();

		// common way by passsing the thread class object
		final Thread thread2 = new Thread(new MyThread2());
		thread2.start();
		System.out.println(thread2.getName() + " thread 2");
		System.out.println(thread2.getPriority());
		System.out.println(thread2.getState());
		// thread2.yield();

		// using thread class
		final Thread thread3 = new Thread();
		thread3.start();
		System.out.println(thread3.getName() + " thread 3");
		// thread3.yield();

		// System.out.println(thread.currentThread().isAlive());

		// another way to create thread
		final Thread thread4 = new MyThread2();

		thread4.start();
		System.out.println(thread4.getName() + " thread 4");
	}

}

//class MyThread implements Runnable {
//
//	@Override
//	public void run() {
//		System.out.println("My thread is running");
//	}
//
//}

class MyThread2 extends Thread {
	@Override
	public void run() {
		System.out.println("My thread 2 is running");
	}

}