package com.wipro.demo.thread;

public class SyncObject {

	Integer val = 0;

//not sync
//	public int incVal() {
//		val++;
//		return val;
//	}

//	// sync
	// public synchronized int incVal() {
//	val++;
//	return val;

//}
	// sync
	public int incVal() {

		synchronized (this) {
			val++;
			return val;
		}

	}

}
