package com.wipro.demo.collection;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

public class IListDemo {

	public static void main(final String[] args) {

		System.out.println("Hello");

		final List<String> list = new ArrayList<>();

		final List<String> strings = new ArrayList<>();

		list.add("Hello");
		list.add(1, "testing index addition");
		list.addAll(strings);

		System.out.println(list.contains("Hello"));
		System.out.println(list);

		list.clear();
		// "Hello".equals(list.get(0));

		list.size();
		list.toArray();
		list.stream().sorted().collect(Collectors.toList());

		final int a = 9;
		final float b = a;
		System.out.println(b);
		System.out.println(a);

		final int arr[] = new int[2];

		arr[1] = 88;
		arr[2] = 77;
		System.out.println(arr[1]);

		final LinkedList<Integer> linkedList = new LinkedList<>();
		linkedList.add(1);
		linkedList.add(2);
		linkedList.add(2);
		linkedList.add(3);
		linkedList.add(4);
		linkedList.add(15);
		linkedList.add(16);

		for (final Integer integer : linkedList) {
			System.out.println(integer);
		}

		final Vector<Integer> vector = new Vector<>();
		vector.add(22);
		vector.add(22);
		vector.add(22);
		vector.add(22);
		vector.add(22);
		vector.add(22);
		vector.add(22);

		vector.stream().forEach(i -> {
			System.out.println(i);
		});

	}

}
