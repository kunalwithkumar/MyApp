package com.wipro.demo.collection;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class SetPractice {

	public static void main(final String[] args) {

		final HashSet<Integer> hashSet = new HashSet<>();

		hashSet.add(1);
		hashSet.add(6);
		hashSet.add(5);
		hashSet.add(4);
		hashSet.add(3);
		hashSet.add(2);

		for (final Integer integer : hashSet) {
			System.out.println(integer);
		}

		final LinkedHashSet<Integer> hashSet2 = new LinkedHashSet<>();

		hashSet2.add(1);
		hashSet2.add(6);
		hashSet2.add(5);
		hashSet2.add(4);
		hashSet2.add(3);
		hashSet2.add(2);

		for (final Integer integer : hashSet2) {
			System.out.println(integer);
		}

		final TreeSet<Integer> treeSet3 = new TreeSet<>();

		treeSet3.add(1);
		treeSet3.add(6);
		treeSet3.add(5);
		treeSet3.add(4);
		treeSet3.add(3);
		treeSet3.add(2);

		for (final Integer integer : treeSet3) {
			System.out.println(integer);
		}
	}

}
