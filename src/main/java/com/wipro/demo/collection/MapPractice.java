package com.wipro.demo.collection;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapPractice {

	public static void main(final String[] args) {

		final HashMap<String, String> map = new HashMap<>();
		map.put("hello", "Greeting");
		map.put("bye", "leaving");
		map.put("love", "emotion");

		for (final String key : map.keySet()) {
			System.out.println(key);
		}

		final LinkedHashMap<String, String> map2 = new LinkedHashMap<>();
		map2.put("hello", "Greeting");
		map2.put("bye", "leaving");
		map2.put("love", "emotion");

		for (final String key : map2.values()) {
			System.out.println(key);
		}

		final TreeMap<String, String> map3 = new TreeMap<>();
		map3.put("hello", "Greeting");
		map3.put("bye", "leaving");
		map3.put("love", "emotion");

		for (final Map.Entry<String, String> mapList : map3.entrySet()) {
			System.out.println(mapList.getKey());
			System.out.println(mapList.getValue());

		}

	}

}
