package com.wipro.demo.series;

public class SumOfSeries {

	public static void main(final String[] args) {

//		Sum of Series
//		SchoolAccuracy: 23.81%Submissions: 93005Points: 0
//		Write a program to find the sum of the given series 1+2+3+ . . . . . .(N terms) 
//
//		Example 1:
//
//		Input:
//		N = 1
//		Output: 1
//		Explanation: For n = 1, sum will be 1.
//		Example 2:
//
//		Input:
//		N = 5
//		Output: 15
//		Explanation: For n = 5, sum will be 1
//		+ 2 + 3 + 4 + 5 = 15.
//		Your Task:
//		Complete the function seriesSum() which takes single integer n, as input parameters and returns an integer denoting the answer. You don't need to print the answer or take inputs.
//
//		Expected Time Complexity: O(1)
//		Expected Auxiliary Space: O(1)
//
//		Constraints:
//		1 <= N <= 105

		// 1+2+3+.....
		final int n = 5;
		// n represent the length of the series
		final long sum = ((long) n * (long) (n + 1)) / 2;

		// 5*6=30
		// 30/2=15

		System.out.println(sum);

	}

}
