package com.wipro.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class ExceptionHandler extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@org.springframework.web.bind.annotation.ExceptionHandler(value = Exception.class)
	public ResponseEntity<Object> exception(final Exception exception) {
		return new ResponseEntity<>("Invalid ID ---> Not found", HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = RuntimeException.class)
	public ResponseEntity<Object> exception2(final RuntimeException exception) {

		return new ResponseEntity<>(exception, HttpStatus.OK);
	}

}

//
//  package com.codility.exceptionhandling; import com.codility.rest.*; import
//  org.springframework.http.*; import org.springframework.web.bind.annotation.*;
//  import org.springframework.web.context.request.*; import
//  org.springframework.web.bind.annotation.ControllerAdvice;
//  
//  // This class can be modified
//  
//  @ControllerAdvice public class ExceptionHandlingControllerAdvice {
//  
//  @org.springframework.web.bind.annotation.ExceptionHandler(value =
//  TaskNotFound.class) public ResponseEntity<Response> exception(TaskNotFound
//  exception) {
//  
//  return new ResponseEntity<>(HttpStatus.NOT_FOUND); }
//  
//  @org.springframework.web.bind.annotation.ExceptionHandler(value =
//  IllegalArgumentException.class) public ResponseEntity<Response>
//  exception(IllegalArgumentException exception) { Response response= new
//  Response(false,"message taken from the exception"); return new
//  ResponseEntity<>(response, HttpStatus.OK);
//  
//  }
//  
//  @org.springframework.web.bind.annotation.ExceptionHandler(value =
//  TaskAlreadyExists.class) public ResponseEntity<Response>
//  exception(TaskAlreadyExists exception) { String title=exception.getTitle();
//  Response response= new
//  Response(false,"Task with title:"+title+" already exists.");
//  
//  return new ResponseEntity<>(response, HttpStatus.OK); }
//  
//  
//  }
//  
//  class Response { private static final boolean FAILURE = false; private final
//  boolean success; private final String message;
//  
//  public Response(boolean success, String message) { this.success = success;
//  this.message = message; }
//  
//  public boolean isSuccess() { return success; }
//  
//  public String getMessage() { return message; }
//  
//  static Response fail(String message) { return new Response(FAILURE, message);
//  } }
//  
// 
//
//package com.codility.exceptionhandling;
//import org.springframework.http.*;
//import org.springframework.web.bind.annotation.*;
//
//// This class can be modified
//
//public class IllegalArgumentException extends RuntimeException {
//    public IllegalArgumentException() {
//        super("message taken from the exception");
//    }
//}
//
//package com.codility.exceptionhandling;
//import org.springframework.http.*;
//import org.springframework.web.bind.annotation.*;
//
//// This class can be modified
//
//public class TaskAlreadyExists extends RuntimeException {
// private String title;
//    public TaskAlreadyExists(String title) {
//        this.title=title;
//    }
// public String getTitle(){return title;}
//}

//
//package com.codility.exceptionhandling;
//import org.springframework.http.*;
//import org.springframework.web.bind.annotation.*;
//
//// This class can be modified
//
//public class TaskNotFound extends RuntimeException {
//    public TaskNotFound(Long id) {
//        super("Task with id: " + id + " not found.");
//    }
//}
//

//import java.util.*;
//
//class Solution {
//    int solution(int X, int Y, int[] A) {
//        int N = A.length;
//        int result = -1;
//        int nX = 0;
//        int nY = 0;
//        for (int i = 0; i < N; i++) {
//            if (A[i] == X)
//                nX += 1;
//             if (A[i] == Y)
//                nY += 1;
//            if ((nX == nY) && (nX!=0 && nY!=0))
//                result = i;
//        }
//        return result;
//    }
//}
