package com.wipro.demo.string;

public class CountOfCamelCaseCharacters {

//	Count of camel case characters
//	SchoolAccuracy: 71.16%Submissions: 20151Points: 0
//	Given a string. Count the number of Camel Case characters in it.
//
//	Example 1:
//
//	Input:
//	S = "ckjkUUYII"
//	Output: 5
//	Explanation: Camel Case characters present:
//	U, U, Y, I and I.
//
//	â€‹Example 2:
//
//	Input: 
//	S = "abcd"
//	Output: 0
//	Explanation: No Camel Case character
//	present.
//
//	Your Task:
//	You don't need to read input or print anything. Your task is to complete the function countCamelCase() which takes the string S as input and returns the count of the camel case characters in the string.
//
//
//	Expected Time Complexity: O(|S|).
//	Expected Auxiliary Space: O(1).

	public static void main(final String[] args) {
		System.out.println(countCamelCase("abcd"));
	}

	static int countCamelCase(final String s) {

		final char arr[] = s.toCharArray();
		int count = 0;
		for (final char element : arr) {
			if ((element >= 'A') && (element <= 'Z')) {
				count++;
			}
		}
		return count;
	}

}
