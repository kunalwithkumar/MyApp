package com.wipro.demo.string;

public class CheckString {

//	Check String
//	SchoolAccuracy: 42.82%Submissions: 16757Points: 0
//	Given a string, check if all its characters are the same or not.
//
//	Example 1:
//
//	Input:
//	s = "geeks"
//	Output: False
//	Explanation: The string contains different
//	character 'g', 'e', 'k' and 's'.
//
//	Example 2:
//
//	Input: 
//	s = "gggg"
//	Output: True
//	Explanation: The string contains only one
//	character 'g'.
//
//	Your Task:
//	You don't need to read input or print anything. Your task is to complete the function check() which takes a string as input and returns True if all the characters in the string are the same. Else, it returns False.
//
//
//	Expected Time Complexity: O(|S|).
//	Expected Auxiliary Space: O(1).

	public static void main(final String[] args) {
		System.out.println(check("aaaaaaaaa"));
	}

	static Boolean check(final String s) {

		final char arr[] = s.toCharArray();
		for (final char element : arr) {
			if (element != s.charAt(0))
				return false;
		}
		return true;
	}

}
