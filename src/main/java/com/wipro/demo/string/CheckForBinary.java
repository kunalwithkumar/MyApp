package com.wipro.demo.string;

public class CheckForBinary {

//	Check for Binary
//	SchoolAccuracy: 30.94%Submissions: 62540Points: 0
//	Given a non-empty sequence of characters str, return true if sequence is Binary, else return false
//
//	Example 1:
//
//	Input:
//	str = 101
//	Output:
//	1
//	Explanation:
//	Since string contains only 0 and 1, output is 1.
//	Example 2:
//
//	Input:
//	str = 75
//	Output:
//	0
//	Explanation:
//	Since string contains digits other than 0 and 1, output is 0.
//	 
//
//	Your Task:
//	Complete the function isBinary() which takes an string str as input parameter and returns 1 if str is binary and returns 0 otherwise.
//

	public static void main(final String[] args) {
		System.out.println(isBinary("01010100"));
		System.out.println(Math.pow(3, 3));
	}

	static boolean isBinary(final String str) {

		for (int i = 0; i < str.length(); i++) {

			if ((str.charAt(i) != '1') && (str.charAt(i) != '0')) {
				System.out.println(str.charAt(i));
				return false;

			}
		}
		return true;

	}

}
