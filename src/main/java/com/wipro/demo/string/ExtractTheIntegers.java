package com.wipro.demo.string;

import java.util.ArrayList;

public class ExtractTheIntegers {

//	Extract the integers
//	SchoolAccuracy: 32.72%Submissions: 7597Points: 0
//	Given a string s, extract all the integers from s.
//
//	Example 1:
//
//	Input:
//	s = "1: Prakhar Agrawal, 2: Manish Kumar Rai, 
//	     3: Rishabh Gupta56"
//	Output: 1 2 3 56
//	Explanation: 
//	1, 2, 3, 56 are the integers present in s.
//	Example 2:
//
//	Input:
//	s = "geeksforgeeks"
//	Output: No Integers
//	Explanation: 
//	No integers present in the string.
//	 
//
//	Your Task:
//	You don't need to read input or print anything. Complete the function extractIntegerWords() which takes string s as input and returns a list of integers. If an empty list is returned the output is printed as "No Integers".
//
//	 
//
//	Expected Time Complexity: O(|s|).
//	Expected Auxiliary Space: O(|s|).
//

	public static void main(final String[] args) {

		System.out.println(extractIntegerWords("1: Prakhar Agrawal, 2: Manish Kumar Rai3: Rishabh Gupta56"));
	}

	static ArrayList<String> extractIntegerWords(final String s) {

		final ArrayList<String> strings = new ArrayList<>();

		for (int i = 0; i < s.length(); i++) {
			if ((s.charAt(i) >= '0') && (s.charAt(i) <= '9')) {
				strings.add(s.charAt(i) + "");
			}
		}
		return strings;
		// code here
	}

}
