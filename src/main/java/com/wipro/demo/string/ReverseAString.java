package com.wipro.demo.string;

public class ReverseAString {

//	Reverse a String
//	SchoolAccuracy: 70.82%Submissions: 28008Points: 0
//	Given a String S , print the reverse of the string as output.
//
//	Example 1:
//
//	Input: S = "GeeksforGeeks"
//	Output: "skeeGrofskeeG" 
//	Explanation: Element at first is at last and
//	last is at first, second is at second last and 
//	second last is at second position and so on .
//	Example 2:
//
//	Input: S = "ReversE"
//	Output: "EsreveR"
//	Explanation: "E" at first and "R" at last and
//	"e" at second last and "s" at second and
//	so on .

	public static void main(final String[] args) {

		System.out.println(revStr("hello World"));

	}

	static String revStr(final String S) {

		return new StringBuilder(S).reverse().toString();
	}

}
