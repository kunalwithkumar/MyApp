package com.wipro.demo.string;

public class ConvertAListOfCharactersIntoAString {

//	Convert a list of characters into a String
//	SchoolAccuracy: 63.69%Submissions: 16673Points: 0
//	Given a list of characters, merge all of them into a string.
//
//	Example 1:
//
//	Input:
//	N = 13
//	Char array = g e e k s f o r g e e k s
//	Output: geeksforgeeks 
//	Explanation: combined all the characters
//	to form a single string.
//
//	Example 2:
//
//	Input:
//	N = 4
//	Char array = e e b a
//	Output: eeba
//	Explanation: combined all the characters
//	to form a single string.
//
//
//	Your Task:
//	You dont need to read input or print anything. Complete the function chartostr() which accepts a char array arr and its size  N  as parameter and returns a string.

	public static void main(final String[] args) {

		final char arr[] = { 'g', 'e', 'e', 'k', 's', 'f', 'o', 'r', 'g', 'e', 'e', 'k', 's' };
		System.out.println(chartostr(arr, arr.length));
	}

	static public String chartostr(final char arr[], final int N) {
		StringBuilder s = new StringBuilder().append(new StringBuilder("").toString());

		for (final char ch : arr) {
			s.append(ch);

		}
		return s.toString();

	}

}
