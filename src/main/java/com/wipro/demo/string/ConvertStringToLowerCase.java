package com.wipro.demo.string;

public class ConvertStringToLowerCase {

//	Convert String to LowerCase
//	SchoolAccuracy: 75.27%Submissions: 36062Points: 0
//	Given a string S. The task is to convert characters of string to lowercase.
//
//	Example 1:
//
//	Input: S = "ABCddE"
//	Output: "abcdde"
//	Explanation: A, B, C and E are converted to
//	a, b, c and E thus all uppercase characters 
//	of the string converted to lowercase letter.
//	Example 2:
//
//	Input: S = "LMNOppQQ"
//	Output: "lmnoppqq"
//	Explanation: L, M, N, O, and Q are 
//	converted to l, m, n, o and q thus 
//	all uppercase characters of the 
//	string converted to lowercase letter.
//	Your Task:  
//	You dont need to read input or print anything. Complete the function toLower() which takes S as input parameter and returns the converted string.
//
//	Expected Time Complexity:O(n)
//	Expected Auxiliary Space: O(1) 

	public static void main(final String[] args) {
		System.out.println(toLower("HelLo "));
	}

	static String toLower(final String S) {
		// code here
		return S.toLowerCase();
	}

}
