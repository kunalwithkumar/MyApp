package com.wipro.demo.string;

public class RemoveSpaces {

//	Remove Spaces
//	SchoolAccuracy: 49.21%Submissions: 41930Points: 0
//	Given a string, remove spaces from it. 
//
//	Example 1:
//
//	Input:
//	S = "geeks  for geeks"
//	Output: geeksforgeeks
//	Explanation: All the spaces have been 
//	removed.
//	Example 2:
//
//	Input: 
//	S = "    g f g"
//	Output: gfg
//	Explanation: All the spaces including
//	the leading ones have been removed.
//
//	Your Task:
//	You don't need to read input or print anything. Your task is to complete the function modify() which takes the string S as input and returns the resultant string by removing all the white spaces from S.

	public static void main(final String[] args) {

		System.out.println(modify("    g f g"));

	}

	static String modify(final String S) {
		return S.replace(" ", "");

	}

}
