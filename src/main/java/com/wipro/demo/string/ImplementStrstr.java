package com.wipro.demo.string;

public class ImplementStrstr {

	public static void main(final String[] args) {
		System.out.println(strstr("geeksforgeeks", "for"));
	}

	static int strstr(final String s, final String x) {
		// Your code here

		return s.indexOf(x);

	}

}
