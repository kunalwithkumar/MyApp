package com.wipro.demo.string;

public class LowerCaseToUpperCase {

//	Lower case to upper case
//	SchoolAccuracy: 74.85%Submissions: 15874Points: 0
//	Given a string str containing only lowercase letters, generate a string with the same letters, but in uppercase.
//
//	Example 1:
//
//	Input:
//	str = "geeks"
//	Output: GEEKS
//	Example 2:
//
//	Input:
//	str = "geeksforgeeks"
//	Output: GEEKSFORGEEKS
//	Your Task:
//	You don't need to read input or print anything. Your task is to complete the function to_upper() which takes the string str as an argument and returns the resultant string.
//
//	Expected Time Complexity: O(length of the string).
//	Expected Auxiliary Space: O(1).
//
//	Constraints:
//	1 ≤ length of the string ≤ 50

	public static void main(final String[] args) {
		System.out.println(to_upper("geeksforgeeks"));
	}

	static String to_upper(final String str) {
		return str.toUpperCase();
	}

}
