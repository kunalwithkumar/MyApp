package com.wipro.demo.string;

public class CountTypeOfCharacters {
//	
//	Count type of Characters
//	SchoolAccuracy: 59.65%Submissions: 14434Points: 0
//	Given a string S, write a program to count the occurrence of Lowercase characters, Uppercase characters, Special characters and Numeric values in the string.
//	Note: There are no white spaces in the string.
//
//	Example 1:
//
//	Input:
//	S = "#GeeKs01fOr@gEEks07"
//	Output:
//	5
//	8
//	4
//	2
//	Explanation: There are 5 uppercase characters,
//	8 lowercase characters, 4 numeric characters
//	and 2 special characters.
//
//	Example 2:
//
//	Input: 
//	S = "*GeEkS4GeEkS*"
//	Output:
//	6
//	4
//	1
//	2
//	Explanation: There are 6 uppercase characters,
//	4 lowercase characters, 1 numeric characters
//	and 2 special characters.
//
//	Your Task:
//	You don't need to read input or print anything. Your task is to complete the function count() which takes the string S as input and returns an array of size 4 where 
	// arr[0] = number of uppercase characters,
	// arr[1] = number of lowercase characters,
	// arr[2] = number of numeric characters and
	// arr[3] = number of special characters.
//
//
//	Expected Time Complexity: O(|S|).
//	Expected Auxiliary Space: O(1).
//

	public static void main(final String[] args) {
		System.out.println(count("*GeEkS4GeEkS*").toString());
	}

	static int[] count(final String s) {
		// your code here
		final int arr[] = new int[4];

		for (int i = 0; i < s.length(); i++) {
			final char ch = s.charAt(i);
			if ((ch >= 'A') && (ch <= 'Z')) {
				arr[0] += 1;
			} else if ((ch >= 'a') && (ch <= 'z')) {
				arr[1] += 1;
			} else if ((ch >= '0') && (ch <= '9')) {
				arr[2] += 1;
			} else {
				arr[3] += 1;
			}
		}

		return arr;
	}

}
