package com.wipro.demo.string;

public class DisplayLongestName {

//	Display longest name
//	SchoolAccuracy: 66.29%Submissions: 45111Points: 0
//	Given a list of names, display the longest name.
//
//
//	Example:
//
//	Input:
//	N = 5
//	names[] = { "Geek", "Geeks", "Geeksfor",
//	  "GeeksforGeek", "GeeksforGeeks" }
//
//	Output:
//	GeeksforGeeks
//	 
//
//	Your Task:  
//	You don't need to read input or print anything. Your task is to complete the function longest() which takes the array names[] and its size N as inputs and returns the Longest name in the list of names.

	public static void main(final String[] args) {

		final String names[] = { "Geek", "Geeks", "Geeksfor", "GeeksforGeek", "GeeksforGeeks" };

		System.out.println(longest(names, names.length));

	}

	static String longest(final String names[], final int n) {
		String longest = "";
		for (int i = 0; i < n; i++) {
			if (names[i].length() > longest.length()) {
				longest = names[i];
			}
		}
		return longest;

	}

}
