package com.wipro.demo.string;

public class MaximumOccuringCharacter {

	public static void main(final String[] args) {
		System.out.println(getMaxOccuringChar("hello"));
	}

	public static char getMaxOccuringChar(final String line) {
		// Your code here

		final char str[] = line.toCharArray();

		final int freq[] = new int[26];

		// to store the maximum occurring character
		char result = '#';
		int max = -1;
		// get frequency of each character of 'str'
		for (int i = 0; i < str.length; i++) {
			if (str[i] != ' ') {
				freq[str[i] - 'a']++;
			}

		}

		// for each character, where character is obtained by
		// (i + 'a') check whether it is the maximum character
		// so far and accordingly update 'result'
		for (int i = 0; i < 26; i++) {

			if (max < freq[i]) {
				max = freq[i];
				result = (char) (i + 'a');
			}
		}
//		for (int i = 0; i < 26; i++) {
//			if (freq[i] == 2) {
//				result = (char) (i + 'a');
//			}
//		}

		// maximum occurring character
		return result;

	}

}
